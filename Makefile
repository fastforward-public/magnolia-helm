RELEASE_YAML=./test-values/base.yml
#CHART_PATH=fastforward/magnolia-helm		# Set to '.' to test local chart
#CHART_VERSION=1.8.4							# Or omit to test local chart
CHART_PATH=.		# Set to '.' to test local chart

export RELEASE=pruning-molly
export SUPERUSER_PW=superuser
export IMAGE_REPOSITORY=registry.gitlab.com/mironet/magnolia-demo
export IMAGE_TAG=latest
export HOSTNAME=pruning-molly.qa.fastforward.ch
export CI_COMMIT_REF_SLUG=dummy

# HELP
# This will output the help for each task
# thanks to https://marmelab.com/blog/2016/02/29/auto-documented-makefile.html
.PHONY: help

help:
	@grep -E '^[a-zA-Z_-]+:.*?## .*$$' $(MAKEFILE_LIST) | sort | awk 'BEGIN {FS = ":.*?## "}; {printf "\033[36m%-30s\033[0m %s\n", $$1, $$2}'

.DEFAULT_GOAL := help

values: ## Show generated yaml resources and values
	cat $(RELEASE_YAML).tpl | gomplate > $(RELEASE_YAML)
	helm install --dry-run --debug -f $(RELEASE_YAML) --generate-name .

clean: ## Clean up environment
	helm del $(RELEASE) || true

clean-pvc: ## Clean disks (PVCs) too
	kubectl get persistentvolumeclaims -l 'release=$(RELEASE)' -o json | kubectl delete -f -

gen-doc: ## Generate docs. Install helm-docs for this to work (https://github.com/norwoodj/helm-docs)
	helm-docs --template-files=docs/README.md.gotmpl

install: ## Install helm chart on k8s
	cat $(RELEASE_YAML).tpl | gomplate > $(RELEASE_YAML)
	helm upgrade -i -f $(RELEASE_YAML) $(RELEASE_ARGS) $(RELEASE) $(CHART_PATH) --version "$(CHART_VERSION)"

release: ## Release helm repo to Chartmuseum
	helm package -u .
	find . -name "magnolia-helm-*.tgz" | xargs -I {} curl -u "$$CHARTMUSEUM_USER:$$CHARTMUSEUM_PASS" --data-binary @{} https://chartmuseum.qa.fastforward.ch/api/charts

	$(eval chart_version := $(shell grep "^version:" Chart.yaml | cut -d':' -f2))
	git tag $(chart_version) && git push -u origin $(chart_version)

test: ## Start helm tests.
	helm unittest . && helm test --logs $(RELEASE)

template: ## Template out, do not send to k8s.
	helm template -f $(RELEASE_YAML) .
