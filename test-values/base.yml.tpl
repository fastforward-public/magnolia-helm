---
ingress:
  enabled: true
  annotations:
    cert-manager.io/cluster-issuer: letsencrypt-staging
  hosts:
    - host: {{ .Env.HOSTNAME }}
      paths:
        - path: /author
          instance: author
        - path: /
          instance: public
  tls:
    - secretName: {{ .Env.CI_COMMIT_REF_SLUG }}-cert
      hosts:
        - {{ .Env.HOSTNAME }}

image:
  tomcat:
    tag: 9.0-jre21-temurin
  pullSecrets:
    - name: gitlab-registry

magnoliaAuthor:
  webarchive:
    repository: {{ .Env.IMAGE_REPOSITORY }}
    tag: {{ .Env.IMAGE_TAG }}
  bootstrap:
    password: {{ env.Getenv "SUPERUSER_PW" "" }}
  resources:
    requests:
      memory: 2Gi
    limits:
      #cpu: 1000m
      memory: 2Gi

magnoliaPublic:
  webarchive:
    repository: {{ .Env.IMAGE_REPOSITORY }}
    tag: {{ .Env.IMAGE_TAG }}
  bootstrap:
    password: {{ env.Getenv "SUPERUSER_PW" "" }}
  resources:
    requests:
      memory: 1.5Gi
    limits:
      #cpu: 1000m
      memory: 1.5Gi
  podDistributionBudget:
    enabled: false

maintenance:
  enabled: true
