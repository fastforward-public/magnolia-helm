stages: [lint, test, regression, release]

process-values:
  stage: lint
  image:
    name: registry.gitlab.com/fastforward-public/helm-kubectl
    entrypoint: [""]
  script:
    #- export HOSTNAME=$(echo $CI_ENVIRONMENT_URL | sed 's:.*/::')
    - export HOSTNAME=${CI_COMMIT_SHORT_SHA}.magnolia-helm.qa.fastforward.ch
    - export IMAGE_REPOSITORY=registry.gitlab.com/mironet/magnolia-demo
    - export IMAGE_TAG=latest
    - for i in test-values/*.tpl; do gomplate --file "$i" --out "test-values/$(basename $i .tpl)"; done
  artifacts:
    paths:
      - test-values/*.yml

##
# Linting & kubeval
##
lint:
  stage: lint
  image:
    name: alpine/helm:3.7.1
    entrypoint: [""]
  script:
    - helm lint

syntax-check:
  stage: lint
  image:
    name: garethr/kubeval:0.15.0
    entrypoint: [""]
  dependencies: [process-values]
  script:
    - for i in `ls test-values/*.yml`; do echo $i; cat $i | kubeval --ignore-missing-schemas; done

unit-tests:
  stage: lint
  image:
    name: registry.gitlab.com/fastforward-public/helm-kubectl:3-16-2-1
  script:
    - helm unittest .

##
# Chart testing
##
.kube_setup: &kube_setup
  - export RELEASE=$CI_ENVIRONMENT_SLUG
  - export K8S_CONTEXT=fastforward-public/magnolia-helm:fastforward-agent-public
  - export K8S_NAMESPACE=helm-chart-testing-dev
  - kubectl config set-context ${K8S_CONTEXT} --namespace ${K8S_NAMESPACE} && kubectl config use-context ${K8S_CONTEXT}
  - kubectl config get-contexts
  - helm repo add mironet https://charts.mirohost.ch
  - helm repo add fastforward https://chartmuseum.qa.fastforward.ch
  - helm repo update

.cleanup_deploy: &cleanup_deploy
  - helm del ${RELEASE} || true
  - kubectl get pods -l component=test -l app.kubernetes.io/instance=${RELEASE} -o json | kubectl delete -f - || true
  - kubectl get persistentvolumeclaims -l "release=${RELEASE}" -o json | kubectl delete -f - || true


.make_deploy: &make_deploy
  - echo "Installing ${RELEASE} (name=${CHART_NAME:-.}, version='${CHART_VERSION:-}') ..."
  - helm upgrade --install ${RELEASE} -f test-values/base.yml -f test-values/${TEST_TYPE}.yml --version "${CHART_VERSION:-}" ${CHART_NAME:-.}
  - helm test --logs ${RELEASE}

.base_stop: &base_stop
  stage: test
  image: registry.gitlab.com/fastforward-public/helm-kubectl
  script:
    - *kube_setup
    - *cleanup_deploy
  environment:
    name: ${TEST_TYPE}/${CI_COMMIT_SHORT_SHA}
    action: stop
  when: manual

##
# Deploy with basic configuration
simple-deploy:
  stage: test
  image:
    name: registry.gitlab.com/fastforward-public/helm-kubectl
  script:
    - *kube_setup
    - *cleanup_deploy
    - *make_deploy
  variables: {TEST_TYPE: simple}
  needs: [process-values]
  dependencies: [process-values]
  environment:
    name: ${TEST_TYPE}/${CI_COMMIT_SHORT_SHA}
    url: https://${TEST_TYPE}-${CI_COMMIT_SHORT_SHA}.magnolia-helm.qa.fastforward.ch
    auto_stop_in: 5 minutes
    on_stop: simple-stop

simple-stop:
  <<: *base_stop
  variables: {TEST_TYPE: simple}

##
# Deploy with advanced configuration
full-deploy:
  stage: test
  image:
    name: registry.gitlab.com/fastforward-public/helm-kubectl
  script:
    - *kube_setup
    - *cleanup_deploy
    - *make_deploy
  variables: {TEST_TYPE: full}
  needs: [process-values]
  dependencies: [process-values]
  environment:
    name: ${TEST_TYPE}/${CI_COMMIT_SHORT_SHA}
    url: https://${TEST_TYPE}-${CI_COMMIT_SHORT_SHA}.magnolia-helm.qa.fastforward.ch
    auto_stop_in: 5 minutes
    on_stop: full-stop
  when: manual

full-stop:
    <<: *base_stop
    variables: {TEST_TYPE: full}

##
# Do regression testing
regression-1.4.4:
  stage: regression
  image:
    name: registry.gitlab.com/fastforward-public/helm-kubectl
  script:
    - *kube_setup
    - *cleanup_deploy
    - *make_deploy
  needs: [process-values]
  dependencies: [process-values]
  variables:
    TEST_TYPE: regression
    CHART_NAME: mironet/magnolia-helm
    CHART_VERSION: 1.4.4
  environment:
    name: ${TEST_TYPE}/${CI_COMMIT_SHORT_SHA}
    url: https://${TEST_TYPE}-${CI_COMMIT_SHORT_SHA}.magnolia-helm.qa.fastforward.ch
  when: manual

regression-deploy:
  stage: regression
  image:
    name: registry.gitlab.com/fastforward-public/helm-kubectl
  script:
    - *kube_setup
    - *make_deploy
  variables: {TEST_TYPE: regression}
  needs: [process-values]
  dependencies: [process-values]
  environment:
    name: ${TEST_TYPE}/${CI_COMMIT_SHORT_SHA}
    url: https://${TEST_TYPE}-${CI_COMMIT_SHORT_SHA}.magnolia-helm.qa.fastforward.ch
    auto_stop_in: 5 minutes
    on_stop: regression-stop
  when: manual

regression-stop:
  <<: *base_stop
  stage: regression
  variables: {TEST_TYPE: regression}
  when: manual


##
# Releasing
##
release:
  stage: release
  image:
    name: alpine/helm:3.16.2
    entrypoint: [""]
  before_script:
    - apk add --upgrade curl
  script: |
    export CHART_VERSION=${CI_COMMIT_REF_NAME}
    echo "Releasing Chart ${CHART_VERSION}..."
    helm package -u . --version ${CHART_VERSION}
    find . -name "magnolia-helm-*.tgz" | xargs -I {} curl -v --fail -u "$CHARTMUSEUM_USER:$CHARTMUSEUM_PASS" --data-binary @{} https://chartmuseum.qa.fastforward.ch/api/charts
  only: [tags]
